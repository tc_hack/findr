var available_options_global = ["Subway Entrances",
                                "Health Centers and Hospitals",
                                "Housing Developments",
                                "School District",
                                "Borough",
                                "Queens Libraries"];

var mapping_to_json = {
    "Subway Entrances": "https://data.cityofnewyork.us/resource/he7q-3hwy.geojson",
    "Health Centers and Hospitals": "https://data.cityofnewyork.us/resource/w7a6-9xrz.geojson",
    "Housing Developments": "https://data.cityofnewyork.us/resource/5j2e-zhmb.geojson",
    "School District": "https://data.cityofnewyork.us/resource/cuae-wd7h.geojson",
    "Borough": "https://data.cityofnewyork.us/resource/7t3b-ywvw.geojson",
    "Queens Libraries": "https://data.cityofnewyork.us/resource/b67a-vkqb.geojson",
}

window.onload = function() {
    localStorage.removeItem("list_of_queries")
    var queensLibs = JSON.parse(httpGet("https://data.cityofnewyork.us/resource/b67a-vkqb.geojson"));
    console.log(queensLibs)
    var features = queensLibs['features'];
    for (i = 0; i < features.length; i++) {
        // console.log(features[i]['properties']['name'])
        var feature_name = "Lib ";
        feature_name += features[i]['properties']['name'];
        available_options_global.push(feature_name)
    }

    var subways = JSON.parse(httpGet("https://data.cityofnewyork.us/resource/he7q-3hwy.geojson"));
    console.log(subways)
    var features = subways['features'];
    for (i = 0; i < features.length; i++) {
        // console.log(features[i]['properties']['name'])
        var feature_name = "Sub ";
        feature_name += features[i]['properties']['name'];
        available_options_global.push(feature_name)
    }

    var borough = JSON.parse(httpGet("https://data.cityofnewyork.us/resource/7t3b-ywvw.geojson"));
    console.log(borough)
    var features = borough['features'];
    for (i = 0; i < features.length; i++) {
        // console.log(features[i]['properties']['name'])
        var feature_name = "Bor ";
        feature_name += features[i]['properties']['boro_name'];
        available_options_global.push(feature_name)
    }

    // (Library) https://data.cityofnewyork.us/resource/b67a-vkqb.geojson
    // (Subway) https://data.cityofnewyork.us/resource/he7q-3hwy.geojson
    // (Police) https://data.cityofnewyork.us/resource/kmub-vria.geojson

    var xhr = $.get("http://api.giphy.com/v1/gifs/random?api_key=dc6zaTOxFJmzC&tag=pug");
    xhr.done(function(data) { 
        $('.gif-bg').css('background-image', 'url(' + data.data.image_url + ')');
    });
}

// function prepareOptions() {
//    var queensLibs = makeOverlay("https://data.cityofnewyork.us/resource/b67a-vkqb.geojson")
//    console.log(queensLibs)
// }

function showOptions(element) {
    var available_options = available_options_global
    var input, filter, ul, li, a, i;
    filter = element.value;
    span = document.getElementById("show_options");

    span.innerHTML = "";

    console.log(filter)
    var count = 0;
    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < available_options.length; i++) {
        if (available_options[i].indexOf(filter) >= 0) {
            var li = document.createElement("button");
            li.setAttribute("class", "btn btn-primary");
            li.setAttribute("onclick", "AddToSearch(this)");
            li.setAttribute("href", "#");
            li.setAttribute("type", "button");
            li.appendChild(document.createTextNode(available_options[i]));
            span.appendChild(li);
            count += 1;
            console.log(count);
        }
        if (count > 3) {
            break;
        }
    }

    if (filter.length == 0) {
        span.innerHTML = "";
    }
}

function showCities(element) {
    var available_options = [
        "Kenvil","Whitehouse","Netcong","Lynbrook","Fair Lawn","Sayreville","Cliffwood","Keansburg","Keyport", "New York",
    ]
    // prepareOptions()

    // Declare variables
    var input, filter, ul, li, a, i;
    filter = element.value;
    ul = document.getElementById("AvailableSerches");

    ul.innerHTML = "";

    console.log(filter)
    var count = 0;
    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < available_options.length; i++) {
        if (available_options[i].indexOf(filter) >= 0) {
            var li = document.createElement("li");
            li.setAttribute("id", "suggested_option");
            li.setAttribute("onclick", "AddToQuery(this)");
            li.setAttribute("href", "#");
            li.setAttribute("type", "button");
            li.appendChild(document.createTextNode(available_options[i]));
            ul.appendChild(li);
            count += 1;
            console.log(count);
        }
        if (count > 3) {
            break;
        }
    }

    if (filter.length == 0) {
        ul.innerHTML = "";
    }
}

function AddToQuery(element) {

    var value = ""
    if(element.id == "add_query_btn") {
        console.log("+");
        value = document.getElementById("query_entry").value;
    }
    else {
        console.log("Suggestion");
        value = element.innerHTML;
    }

    if (value.length == 0) {
        console.log("Zero Length");
        return;
    }

    console.log(value);

    var element_to_add_to_list = document.createElement("a");
    element_to_add_to_list.setAttribute("href", "#");
    element_to_add_to_list.setAttribute("class", "list-group-item");
    var element_text = document.createTextNode(value);
    list_div = document.getElementById("added_queries");
    element_to_add_to_list.appendChild(element_text);
    list_div.appendChild(element_to_add_to_list)
    document.getElementById("process_query_button").disabled = false;

    var data = localStorage.getItem("list_of_queries")
    console.log(data)
    console.log(typeof(data))
    if (data) {
        data += "%"
        data += value
        localStorage.setItem("list_of_queries", data)
    }
    else {
        localStorage.setItem("list_of_queries", value)
    }
}


function AddToSearch(element) {
    var overlay_list = ["Queens Libraries", "Subway Entrances", "Health Centers and Hospitals"]
    var boundary_list = ["Housing Developments", "Borough", "School District"]
    var value = ""
    value = element.innerHTML;
    console.log(value);
    if (value in mapping_to_json) {
        console.log("Calling");
        console.log(mapping_to_json[value]);
        if (overlay_list.indexOf(value) >= 0) {
            console.log("Calling Overlay");
            setOverlay(mapping_to_json[value], value, ('#'+(Math.random()*0xFFFFFF<<0).toString(16)));
        }
        else {
            console.log("Calling Boundary");
            setBoundary(mapping_to_json[value], value, ('#'+(Math.random()*0xFFFFFF<<0).toString(16)));
        }
    }
    else {
        var res = encodeURIComponent(value.slice(4));
        console.log(res);
        if (value.indexOf("Bor ") >= 0) {
            var updated_q = mapping_to_json["Borough"];
            updated_q += "?boro_name='";
            updated_q += res;
            updated_q += "'";
            console.log(updated_q);
            setBoundary(updated_q, res, ('#'+(Math.random()*0xFFFFFF<<0).toString(16)));
        }
        // return
    }
}

function ProcessQuery(element) {
    console.log(localStorage.getItem("list_of_queries"));
    window.location.href = '/leaf.html'; //relative to domain
    return;
}

require([
        "esri/Map",
        "esri/views/MapView",

        "esri/layers/FeatureLayer",

        "dojo/domReady!"
      ],
      function(
        Map, MapView,
        FeatureLayer
      ) {


        var list_of_queries = localStorage.getItem("list_of_queries");
        if (list_of_queries) {
            list_of_queries = list_of_queries.split('%');
            console.log("Mapping Time");
            var arrayLength = list_of_queries.length;
            for (var i = 0; i < arrayLength; i++) {
                console.log(list_of_queries[i]);
            }
        }

        var map = new Map({
          basemap: "streets"
        });

        var view = new MapView({
          container: "viewDiv",
          map: map,

          extent: { // autocasts as new Extent()
            xmin: -9177811,
            ymin: 4247000,
            xmax: -9176791,
            ymax: 4247784,
            spatialReference: 102100
          }
        });

        /********************
         * Add feature layer
         ********************/

        // Carbon storage of trees in Warren Wilson College.
        var featureLayer = new FeatureLayer({
          url: "https://services.arcgis.com/V6ZHFr6zdgNZuVG0/arcgis/rest/services/Landscape_Trees/FeatureServer/0"
        });

        map.add(featureLayer);

      });


function httpGet(theUrl)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
    xmlHttp.send( null );
    return xmlHttp.responseText;
}