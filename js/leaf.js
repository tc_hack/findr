   function stuff(data) {
      return L.geoJson(data, {
        pointToLayer: function( feature, latlng) {
          var circle = L.circleMarker(latlng, {
            radius: 6,
            fillColor: "orange",
            color: "orange",
            weight: 2,
            opacity: 1,
            fillOpacity: 0.7
        });
          circle.bindPopup(feature.properties.name);
          return circle;
      }
  }).addTo(map);
      controlLayers.addOverlay(geoJson, 'Queens Libraries');
  }

  function makeOverlay(query) {
      return $.getJSON(query, stuff);
  }

  function makeBase() {
      return L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
        subdomains: 'abcd',
        maxZoom: 19
    });
  }

  var map = L.map('map').setView([40.7128, -74.0059], 12);

  var controlLayers = L.control.layers( null, null, {
      position: "topright",
      collapsed: false
  }).addTo(map);
  
  var base = makeBase();
  base.addTo(map)
  controlLayers.addBaseLayer(base, 'NYC');

  var markers = []

   $.getJSON("https://data.cityofnewyork.us/resource/b67a-vkqb.geojson", function (data){
     var geoJsonLayer = L.geoJson(data, {
       pointToLayer: function( feature, latlng) {
        markers.push(new L.marker(latlng))
        return markers[markers.length-1];
         // var circle = L.circleMarker(latlng, {
         //   radius: 6,
         //   fillColor: "blue",
         //   color: "blue",
         //   weight: 2,
         //   opacity: 1,
         //   fillOpacity: 0.7
         // });
         // circle.bindPopup(feature.properties.name);
         // return circle;
       }
     }); // insert ".addTo(map)" to display layer by default
     controlLayers.addOverlay(geoJsonLayer, 'Queens Libraries');
   });

   $.getJSON("https://data.cityofnewyork.us/resource/he7q-3hwy.geojson", function (data){
     var geoJsonLayer = L.geoJson(data, {
       pointToLayer: function( feature, latlng) {
        markers.push(new L.marker(latlng))
        return markers[markers.length-1];
         // var circle = L.circleMarker(latlng, {
         //   radius: 6,
         //   fillColor: "orange",
         //   color: "orange",
         //   weight: 2,
         //   opacity: 1,
         //   fillOpacity: 0.7
         // });
         // circle.bindPopup(feature.properties.business + '<br>' + feature.properties.item); // replace last term with property data labels to display from GeoJSON file
         // return circle;
       }
     }); // insert ".addTo(map)" to display layer by default
     controlLayers.addOverlay(geoJsonLayer, 'Subway Entrances');
   });

   $.getJSON("https://data.cityofnewyork.us/resource/kmub-vria.geojson", function (data){
     var geoJsonLayer = L.geoJson(data, {
       pointToLayer: function( feature, latlng) {
        markers.push(new L.marker(latlng))
        return markers[markers.length-1];
         // var circle = L.circleMarker(latlng, {
         //   radius: 6,
         //   fillColor: "orange",
         //   color: "orange",
         //   weight: 2,
         //   opacity: 1,
         //   fillOpacity: 0.7
         // });
         // circle.bindPopup(feature.properties.business + '<br>' + feature.properties.item); // replace last term with property data labels to display from GeoJSON file
         // return circle;
       }
     }); // insert ".addTo(map)" to display layer by default
     controlLayers.addOverlay(geoJsonLayer, 'Police Precints');
   });

   function checkMarkersWithinBounds(bounds, markers){
     for(var i = 0; i < markers.length; i++){
        if (bounds.contains(L.latLng(markers[i].getLatLng().lat, 
           markers[i].getLatLng().lng))){
           markers[i].setOpacity(1);
        }
       else{
           markers[i].setOpacity(0);
       }
     }
   }

   var locationFilter = new L.LocationFilter().addTo(map);
   locationFilter.on("change", function (e) {
       checkMarkersWithinBounds(e.bounds, markers);
   });
   locationFilter.on("enabled", function () {
       checkMarkersWithinBounds(locationFilter.getBounds(), markers);
   });
   locationFilter.on("disabled", function () {
       for(var i = 0; i < markers.length; i++){
         markers[i].setOpacity(0);
         }
   });
